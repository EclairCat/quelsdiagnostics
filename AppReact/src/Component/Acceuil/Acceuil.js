import React from "react";

//Bootstrap
import Image from 'react-bootstrap/Image';


//component
import { Container, Row, Button, Col } from "react-bootstrap";

export class Acceuil extends React.Component {
    render() {
        return (
            <>
                <Image src="require('../../../Assets/Images/Building.jpg')" fluid />
                <Container >
                    <Row>
                        <Col>N°1 de l’estimation des diagnostics immobiliers</Col>
                    </Row>
                    <Row>
                        <Col>Estimez gratuitement vos diagnostics obligatoire en 5 minutes</Col>
                    </Row>
                    <Row>
                        <Col><Button variant="success">Commencer L'estimation</Button></Col>
                    </Row>
                </Container>

            </>
        )
    }
}