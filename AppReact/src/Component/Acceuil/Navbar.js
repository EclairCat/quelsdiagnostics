import React from 'react';
import { Redirect } from "react-router-dom";
import './Navbar.css'

import { Button, Navbar, Nav, Col } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'

export class MenuBar extends React.Component {

    constructor() {
        super() 
        this.unlog = this.unlog.bind(this);
    }

    unlog(){
        sessionStorage.removeItem("jwtToken");        
    }
  
    render() {   
        
        let navLoggedin;
        if(this.props.loggedIn){
            navLoggedin = <Nav>                                              
                <Nav.Link><NavLink className="linkNav" to="/Espace_Agence">Espace Agence</NavLink></Nav.Link>
                <Button variant="danger" onClick={this.unlog} >Déconnexion</Button>
            </Nav>
        } else {
            navLoggedin = <Nav>                                              
                <Nav.Link><NavLink className="linkNav" to="/Connexion">Connexion</NavLink></Nav.Link>
                <Nav.Link><NavLink className="linkNav" to="/Inscription">Inscription</NavLink></Nav.Link>
            </Nav>
        }

        return (
            <>               
                <Navbar bg="dark">
                    <Navbar.Brand>    
                            <NavLink className="linkNav" to="/">QuelDiagnostict</NavLink>
                    </Navbar.Brand>                    
                    <Navbar.Collapse> 
                        <Nav className="mr-auto">                                    
                        <Nav.Link><NavLink className="linkNav" to="/Estimation"> Estimer un bien </NavLink></Nav.Link>
                        <Nav.Link><NavLink className="linkNav" to="/Diagnostics">Prix Diagnostics</NavLink></Nav.Link>
                        </Nav>
                    {navLoggedin}
                    </Navbar.Collapse>
                </Navbar>
            </>
        )
    }
}