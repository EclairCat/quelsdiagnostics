import React from 'react';

//Router
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";

//Components
import { Acceuil } from './Acceuil/Acceuil';
import { Login } from './Auth/Login';
import { MenuBar } from './Acceuil/Navbar';
import { Error404 } from './Error404';
import { Form_estimation } from './Estimation/Form_Estimation';
import { Form_diagnostics } from './Diagnostics/Form_Diagnostics';
import { Inscription } from './Auth/Inscription';

import { Container, Button, Navbar, Nav, Col } from 'react-bootstrap';
import { NavLink } from 'react-router-dom'

export class App extends React.Component {

    constructor() {
        super()

        this.state = {
            loggedIn: false,
        }

        this.appLogin = this.appLogin.bind(this);
    }

    appLogin(islogged){
        console.log("hewo")
        this.setState({
            loggedIn: islogged
        })
    }

    render() {   
        
        let i = true;
        return (
            <>
                <Router>
                    <MenuBar loggedIn = {this.state.loggedIn} />
                    
                    <Switch>
                        <Route path="/" component={Acceuil} exact/>
                        <Route appLogin={this.appLogin} path="/Connexion" component={Login}/>
                        <Route path="/Estimation" component={Form_estimation}/>
                        <Route path="/Diagnostics" component={Form_diagnostics}/>
                        <Route path="/Inscription" component={Inscription}/>
                        <Route component={Error404} />
                    </Switch>
                    
                </Router>                
            </>
        )
    }
}