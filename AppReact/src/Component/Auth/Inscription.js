import React from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
//Bootstrap
import { Form, Button, Container, Alert } from "react-bootstrap";

//component


export class Inscription extends React.Component {



    constructor(prop) {
        super(prop);


        this.state = {
            email: "",
            password: "",
            password_confirmation: "",
            typeOfUser: 0,
            messageSubmitBool: 0,
            redirection: false,
        };



        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }

    handleSubmit(event) {

        event.preventDefault();

        const { email, password, password_confirmation, typeOfUser } = this.state;

        console.log(this.state);

        if (password == password_confirmation) {
            axios.post(
                'http://localhost:4000/api/auth/inscription',
                {
                    email: email,
                    mdp: password,
                    typeOfUser: typeOfUser,
                },
                { headers: { 'Content-Type': 'application/json' } }
            )
                .then(res => {
                    //Status a mettre en 201 car reactjs ne capte que 404
                    if (res.status = 200) {
                        //this.state.messageSubmitBool = 1; 
                        if (window.confirm("Inscription Réussit!")) {
                            this.setState({
                                redirection : true,
                                messageSubmitBool: 1
                            });
                        };
                    }
                    console.log(res);
                    console.log(this.state);
                })
                .catch(error => {
                    this.setState({
                        messageSubmitBool: 2
                    });
                    //window.confirm("Erreur veuillez reessayer");
                    console.log(this.state);
                })
        } else {
            this.setState({
                messageSubmitBool: 2
            });
            //window.confirm("Les mot de passe ne sont pas égaux");            
        }
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        }); 
    }

    render() {

        //AlertBox après Submit du formulaire
        let messageAfterSubmit;
        if (this.state.messageSubmitBool == 1) {
            messageAfterSubmit = <Alert show={true} variant="success">
                <Alert.Heading>Inscription Réussit!</Alert.Heading>
            </Alert>
        } else if (this.state.messageSubmitBool == 2) {
            messageAfterSubmit = <Alert show={true} variant="danger">
                <Alert.Heading>Erreur d'inscription, veuillez réessayer.</Alert.Heading>
            </Alert>
        };

        //Redirection
        const { redirection } = this.state;
        if (redirection) {
            //Affichage de la redirection
            return <Redirect to='/' />
        };

        return (
            <>
                <Container>
                    <h4>Inscription</h4>
                    <Form onSubmit={this.handleSubmit} className="mr-4">
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Adresse Email</Form.Label>
                            <Form.Control name="email" value={this.state.email} onChange={this.handleChange} type="email" placeholder="Email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Mot de Passe</Form.Label>
                            <Form.Control name="password" value={this.state.password} onChange={this.handleChange} type="password" placeholder="Mot de passe" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Confirmation du Mot de Passe</Form.Label>
                            <Form.Control name="password_confirmation" value={this.state.password_confirmation} onChange={this.handleChange} type="password" />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Vous êtes :</Form.Label>
                            <div>
                                <Form.Check inline name="typeOfUser" type="radio" value="1" onChange={this.handleChange} label="Une Agence Immobilier" />
                                <Form.Check inline name="typeOfUser" type="radio" value="2" onChange={this.handleChange} label="Un Diagnostiqueur" />
                            </div>                            
                        </Form.Group>
                        

                        <Button variant="primary" type="submit">
                            Valider
                        </Button>
                    </Form>
                    {messageAfterSubmit}
                </Container>
            </>
        )
    }
}