import React from "react";
import axios from "axios";
import { PropTypes } from 'react';
//Bootstrap
import { Form, Button, Container, Alert } from "react-bootstrap";
import { Redirect } from "react-router";

//component
export class Login extends React.Component {
    constructor(prop) {
        super(prop);


        this.state = {
            email: "",
            password: "",            
            redirection: false,
            messageSubmitBool: 0,
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const { email, password } = this.state;

        console.log(this.state);

        axios.post(
            'http://localhost:4000/api/auth/login',
            {
                email: email,
                mdp: password,
            },
            { headers: { 'Content-Type': 'application/json' } }
        )
            .then(res => {                
                sessionStorage.setItem("jwtToken", res.data.token);
                console.log(res);
                console.log(this.state);
                if (window.confirm("Connexion réussit!")) {
                    //this.props.appLogin(true);
                    this.setState({                        
                        redirection: true,
                        messageSubmitBool: 1
                    })                    
                };
            })
            .catch(error => {
                console.error(error);
                this.setState({
                    messageSubmitBool: 2
                });
            })

    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });

    }

    render() {

        //AlertBox après Submit du formulaire
        let messageAfterSubmit;
        if (this.state.messageSubmitBool == 1) {
            messageAfterSubmit = <Alert show={true} variant="success">
                <Alert.Heading>Connexion Réussit!</Alert.Heading>
            </Alert>
        } else if (this.state.messageSubmitBool == 2) {
            messageAfterSubmit = <Alert show={true} variant="danger">
                <Alert.Heading>Erreur, l'email ou le mot de passe sont invalide.</Alert.Heading>
            </Alert>
        };

        //Redirection
        const { redirection } = this.state;
        if (redirection) {
            //Affichage de la redirection
            return <Redirect to='/' />
        };

        return (
            <>
                <Container>
                    <h4>Connexion</h4>
                    <Form onSubmit={this.handleSubmit} className="mr-4">
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Adresse Email</Form.Label>
                            <Form.Control name="email" value={this.state.email} onChange={this.handleChange} type="email" placeholder="Email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Mot de Passe</Form.Label>
                            <Form.Control name="password" value={this.state.password} onChange={this.handleChange} type="password" placeholder="Mot de passe" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Valider
                        </Button>
                    </Form>
                    {messageAfterSubmit}
                </Container>
            </>
        )
    }
}