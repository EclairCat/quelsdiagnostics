import React from "react";

//Bootstrap
import Image from 'react-bootstrap/Image';

//component
import { Container, Row, Button, Col } from "react-bootstrap";

export class Error404 extends React.Component {
    render() {
        return (
            <>
                <div>
                    <p>
                        Erreur 404 : la page n'existe pas!
                    </p>
                </div>
            </>
        )
    }
}