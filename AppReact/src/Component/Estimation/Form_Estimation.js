import React from "react";

//Bootstrap
import { Form, Button, Container, } from "react-bootstrap";

//component


export class Form_estimation extends React.Component {
    render() {
        return (
            <>
                <Container>
                <h4>Estimation d'un bien</h4>
                    <Form className="mr-4">
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Adresse Email</Form.Label>
                            <Form.Control type="email" placeholder="Email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Mot de Passe</Form.Label>
                            <Form.Control type="password" placeholder="Mot de passe" />
                        </Form.Group>
                        <Button variant="primary" type="submit">
                            Valider
                        </Button>
                    </Form>
                </Container>
            </>
        )
    }
}