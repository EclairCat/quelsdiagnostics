//Imports
var express = require('express');
var bodyParser = require('body-parser');
var apiRouter = express.Router();
var cors = require('cors');
var jwt = require('jsonwebtoken');

//Import Controller 
var test = require("./Controller/test");
var authentification = require("./Controller/Authentification");
var auth = require ("./middleware/auth");


//Fonction qui Verifie le Token d'un Client 
/*
function verifyTokenClient(req, res, next) {
    if(!req.headers['authorization']){
        return res.status(401).send("Unothorize Request 1");
    }
    let token = req.headers['authorization'].split(' ')[1];
    if(token === 'null'){
        return res.status(401).send("Unothorize Request 2");
    }
    try {
        let payload = jwt.verify(token, clientKeyToken);
        next();
        
    } catch (error) {
        return res.status(401).send("Unothorize Request 3");
    }   
    
}*/


// Router
exports.router = (function () {
    //Config api
    apiRouter.use(bodyParser());
    apiRouter.use(cors());    
   
    //Exemple de route : apiRouter.route('/rdv/denieRdv').post(verifyTokenMedecin, rdvController.denieRdv);
    apiRouter.route('/test').get(test.test);

    //Authentificaiton
    apiRouter.route('/auth/inscription').post(authentification.inscription);
    apiRouter.route('/auth/login').post(authentification.login);
    apiRouter.route('/getAllUser').get(auth, authentification.getAllUser);

    return apiRouter;
})();