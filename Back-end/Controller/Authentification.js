//Imports
var db = require("../dbb");
var bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');


const User = require('../Models/User');

module.exports = {

    //Les Différentes Fonctions

    //Ajoute un User a la BDD
    inscription: function (req, res) {
        console.log("ROUTE Auth -- Inscription");

        bcrypt.hash(req.body.mdp, 10)
            .then(hash => {
                const user = new User({
                    email: req.body.email,
                    mdp: hash,
                    typeOfUser: req.body.typeOfUser,
                });
                user.save()
                    .then(() => res.status(201).json({ message: 'Utilisateur créé !' }))
                    .catch(error => res.status(400).json({ error }));
            })
            .catch(error => res.status(500).json({ error }));
    },

    //Vérifie le user et le connecte au site en renvoyer un token et son id
    login: function (req, res) {
        console.log("ROUTE Auth -- Login");

        User.findOne({ email: req.body.email })
            .then(user => {
                if (!user) {
                    return res.status(401).json({ error: 'Utilisateur non trouvé !' });
                }
                bcrypt.compare(req.body.mdp, user.mdp)
                    .then(valid => {
                        if (!valid) {
                            return res.status(401).json({ error: 'Mot de passe incorrect !' });
                        }
                        res.status(200).json({
                            userId: user._id,                            
                            token: jwt.sign(
                                { userId: user._id },
                                'RANDOM_TOKEN_SECRET',
                                { expiresIn: '24h' }
                            )
                        });
                    })
                    .catch(error => res.status(500).json({ error }));
            })
            .catch(error => res.status(500).json({ error }));
    },

    //Récupère tout les utilisateurs de la BDD
    getAllUser: function (req, res) {
        console.log("ROUTE Auth -- Get all User");

        User.find()
            .then(users => res.status(200).json(users))
            .catch(error => res.status(400).json({ error }));
    }

}